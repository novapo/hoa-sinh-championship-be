const router = require('express').Router();
const auth = require('../auth/passport.strategy')();
const { checkPermission } = require('../auth/auth.permission');
const examCtl = require('./exam.controller');
const {
    validateExamCreate,
    validateExamUpdate,
} = require('./exam.validate');

router.use(auth.authenticate);

router
    .route('/')
    .get(checkPermission(['admin', 'editor']), examCtl.getAll)
    .post(
        checkPermission(['admin', 'editor']),
        validateExamCreate,
        examCtl.create,
    );
router
    .route('/search')
    .get(checkPermission(['admin']), examCtl.searchByName);
    
router
    .route('/:id/questions/search')
    .get(checkPermission(['admin']), examCtl.searchQuestionInExamByContent);

router
    .route('/:id/questions')
    .get(checkPermission(['admin']), examCtl.questionList);

router
    .route('/:id')
    .get(checkPermission(['admin', 'editor']), examCtl.getById)
    .patch(
        checkPermission(['admin', 'editor']),
        validateExamUpdate,
        examCtl.updateById,
    )
    .delete(checkPermission(['admin', 'editor']), examCtl.deleteById);



module.exports = router;
