/* eslint-disable no-param-reassign */
const Joi = require('@hapi/joi');
const { schemaValidator } = require('../../common/schema-validator/utils');

// We only require some field when create new exam...
const examCreateSchema = Joi.object({
    name: Joi.string().min(1).required(),
    maxScore: Joi.number().required(),
    totalQuestionUserMustDo: Joi.number().required(),
}).unknown(true);

//  ...but not when we update its data.
const examUpdateSchema = Joi.object({
    name: Joi.string().min(1),
    maxScore: Joi.number(),
    totalQuestionUserMustDo: Joi.number(),
}).unknown(true);

module.exports = {
    // Use for create
    validateExamCreate: function (req, res, next) {
        try {
            delete req.body.countQuestion
            schemaValidator(examCreateSchema, req.body);

            // We dont need id, which is automatically generated by mysql
            delete req.body.id;

            return next();
        } catch (err) {
            return next(err);
        }
    },

    // Use for update
    validateExamUpdate: function (req, res, next) {
        try {
            delete req.body.countQuestion
            schemaValidator(examUpdateSchema, req.body);

            // We dont need id, which is automatically generated by mysql
            // delete req.body.id;

            return next();
        } catch (err) {
            return next(err);
        }
    },
};
