const router = require('express').Router();
const adminCtrl = require('./admin.controller');
const roleCtrl = require('./role.controller');
const auth = require('../auth/passport.strategy')();
const { checkPermission } = require('../auth/auth.permission');
const {
    validateUpdate,
} = require('./admin.validate');

router.use(auth.authenticate);

/**
 * -------------- FOR ADMINS ----------------
 */

router.get('/me', checkPermission(['user', 'editor']), adminCtrl.whoAmI);
router.patch(
    '/',
    checkPermission(['admin', 'editor']),
    validateUpdate,
    adminCtrl.updateById,
);

router.post('/create', checkPermission(['admin', 'editor']), adminCtrl.create);

router.get('/role', checkPermission(['admin', 'editor']), roleCtrl.getAll);

router.get('/', checkPermission(['admin', 'editor']), adminCtrl.getAll);
router.get('/:id', checkPermission(['admin', 'editor']), adminCtrl.getById);

router.delete('/:id', checkPermission(['admin', 'editor']), adminCtrl.deleteById);


module.exports = router;
