const models = require('../../models');
const AppError = require('../../common/error/error');
const { httpStatus } = require('../../common/error/http-status');
const wrapCatchAsync = require('../../common/error/catchAsync'); 

module.exports = {
  
    getAll: wrapCatchAsync(async function (req, res, next) {
      const roles = await models.role.findAndCountAll();
      return res.json(roles);
    }),

    create: wrapCatchAsync(async function (req, res, next) {
        const role = await models.role.create(req.body);

        return res.json(role);
    }),

    getById:  wrapCatchAsync(async function (req, res, next) {
      const { id } = req.params;
      const found = await models.role.findByPk(id);

      if (!found) {
          throw new AppError(
              httpStatus.NOT_FOUND,
              'This role does not exist.',
              true,
          );
      }
      return res.json(found);
    }),

    updateById: wrapCatchAsync(async function (req, res, next) {
      const { id } = req.params;
      const role = await models.role.findByPk(id);
      role.set(req.body);
      const updated = await role.save();

      return res.json(updated);
    }),

    deleteById: wrapCatchAsync(async function (req, res, next) {
      const { id } = req.params;  
      const affected = await models.role.destroy({
            where: {
                id: id,
            },
        });

      if (affected === 0) {
          throw new AppError(
              httpStatus.NOT_FOUND,
              'This role does not exist.',
              true,
          );
      }

      return res.json(affected);
    }),
};
