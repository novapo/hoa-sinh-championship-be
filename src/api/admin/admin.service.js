/* eslint-disable camelcase */
const models = require('../../models');
const AppError = require('../../common/error/error');
const { httpStatus } = require('../../common/error/http-status');

module.exports = {
    whoAmI: async function (id) {
        const user = await this.getUserById(id);

        delete user.dataValues.password;
        return user;
    },

    create: async function (body) {
        const user = await models.admin.create(body);
        return user;
    },

    updateById: async function (id, body) {
        const user = await this.getById(id);
        user.set(body);
        const updated = await user.save();

        return updated;
    },

    getById: async function (id) {
        const found = await models.admin.findByPk(id);

        if (!found) {
            throw new AppError(
                httpStatus.NOT_FOUND,
                'This administrator does not exist.',
                true,
            );
        }

        return found;
    },

    deleteById: async function (id) {
        const deleted = await models.admin.destroy({
            where: {
                id: id,
            },
        });

        if (deleted === 0) {
            throw new AppError(
                httpStatus.NOT_FOUND,
                'This administrator does not exist.',
                true,
            );
        }

        return deleted;
    },

    getAll: async function () {
        const info = await models.admin.findAndCountAll({});
        return info;
    },
};
