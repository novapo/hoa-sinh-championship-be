/* eslint-disable no-param-reassign */
const Joi = require('@hapi/joi');
const { schemaValidator } = require('../../common/schema-validator/utils');
const { rolesEnum } = require('./role.enum');

const rolesSchema = Joi.object({
    role: Joi.valid(...rolesEnum).required(),
}).unknown(true);

const idListSchema = Joi.object({
    ids: Joi.array().required().items(Joi.number()),
}).unknown(true);

const userCreateSchema = Joi.object({
    code: Joi.string().min(6).max(255).required(),
    sessionId: Joi.number().required(),
}).unknown(true);

const userUpdateSchema = Joi.object({
    sessionId: Joi.number(),
}).unknown(true);

module.exports = {
    validateUserCreate: function (req, res, next) {
        try {
            schemaValidator(userCreateSchema, req.body);

            delete req.body.id;
            delete req.body.roleId;
            delete req.body.isActive;
            delete req.body.historyQues;
            delete req.body.historyAnss;
            
            next();
        } catch (err) {
            next(err);
        }
    },
    validateUserUpdate: function (req, res, next) {
        try {
            schemaValidator(userUpdateSchema, req.body);

            delete req.body.id;
            delete req.body.code;
            delete req.body.roleId;
            delete req.body.historyQues;
            delete req.body.historyAnss;
            delete req.body.universityId;
            
            next();
        } catch (err) {
            next(err);
        }
    },

    validateRoleUpdate: function (req, res, next) {
        try {
            schemaValidator(rolesSchema, req.body);

            next();
        } catch (err) {
            next(err);
        }
    },

    validateActivationUpdate: function (req, res, next) {
        try {
            schemaValidator(idListSchema, req.body);

            next();
        } catch (err) {
            next(err);
        }
    },
};
