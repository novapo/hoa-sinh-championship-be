const models = require('../../models');
const AppError = require('../../common/error/error');
const { httpStatus } = require('../../common/error/http-status');

module.exports = {
    getAll: async function (page = 0, pageSize = 10) {
        const pageNum = parseInt(page, 10);
        const pageSizeNum = parseInt(pageSize, 10);
        const offset = pageNum * pageSizeNum;
        const limit = pageSizeNum;

        const questions = await models.session.findAndCountAll({
            limit,
            offset,
            where: {}, // conditions
        });
        return questions;
    },

    searchByName: async function (name = '') {
        if (name === '') return [];

        const sql = `SELECT *
                FROM sessions
                WHERE sessions.name LIKE :name `;
        const sessions = await models.sequelize.query(sql, {
            replacements: { name: `%${name}%` },
            type: models.sequelize.QueryTypes.SELECT,
        });

        return sessions;
    },

    create: async function (data) {
        const session = await models.session.create(data);

        return session;
    },

    getById: async function (id) {
        console.log(id);
        const found = await models.session.scope('type','exam').findByPk(id);

        if (!found) {
            throw new AppError(
                httpStatus.NOT_FOUND,
                'This session does not exist.',
                true,
            );
        }

        return found;
    },

    updateById: async function (id, body) {
        const session = await this.getById(id);
        session.set(body);
        const updated = await session.save();

        return updated;
    },

    isCanActive: async function (id) {
        const session = await this.getById(id);
        if (!session.isActive && !session.finalTime) { return session; }
        return false;
    },
    isCanExam: async function (id) {
        const session = await this.getById(id);
        if (session.isActive && !session.finalTime) { return session; }
        return false;
    },

    startSessionById: async function (id) {
        const session = await this.getById(id);
        session.set({isActive: true, finalTime: null});
        const updated = await session.save();
        return updated
    },

    finalSessionById: async function (id, timeServer) {
        const session = await this.getById(id);
        session.set({isActive: false, finalTime: timeServer});
        const updated = await session.save();

        return updated;
    },

    deleteById: async function (id) {
        const affected = await models.session.destroy({
            where: {
                id: id,
            },
        });

        if (affected === 0) {
            throw new AppError(
                httpStatus.NOT_FOUND,
                'This session does not exist.',
                true,
            );
        }

        return affected;
    },
};
