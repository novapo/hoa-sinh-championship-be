module.exports = (sequelize, Sequelize) => {
  class Session extends Sequelize.Model {}

  Session.init(
      {
        typeId: {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        name: {
            type: Sequelize.STRING(50),
            allowNull: false,
        },
        isActive: {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
          allowNull: false,
        },
        finalTime: {
          type: Sequelize.DATE,
          defaultValue: null,
          allowNull: true,
        },
        examId: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      },
      {
          sequelize,
          modelName: 'session',
          timestamps: false,
          underscored: true, //  Set the field option on all attributes to the snake_case version of its name
      },
  );

  /**
   * -------------- ASSOCIATION ----------------
   */
  
  Session.associate = function (models) {
      Session.belongsTo(models.exam, {
          foreignKey: 'examId',
          as: 'exam',
          // onDelete: 'CASCADE',
      });
      Session.belongsTo(models.type, {
        foreignKey: 'typeId',
        as: 'type',
        // onDelete: 'SET NULL',
      });
      Session.addScope('type', {
        include: [
            {
                model: models.type,
                as: 'type',
                required: true,
                attributes: {
                    include: ['id', 'description'],
                },
            },
        ],
      });
      Session.addScope('exam', {
        include: [
            {
                model: models.exam,
                as: 'exam',
                required: true,
                attributes: {
                    include: ['id', 'name', 'maxScore', 'totalQuestionUserMustDo'],
                },
            },
        ],
    });
  };

  return Session;
};